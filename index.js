const app = require("express")();
const http = require("http").Server(app);
const request = require("request");
const port = process.env.PORT || 3000;

// API specification: https://feedback.weather.com/customer/en/portal/articles/2924682-pws-upload-protocol?b_id=17298

const apiRootUrl = 'https://weatherstation.wunderground.com/weatherstation/updateweatherstation.php';

const apiUrlQueryParms = {
    ID: process.env.ID,
    PASSWORD: process.env.PASSWORD,
    dateutc: 'now',
    action: 'updateraw',
    softwaretype: 'gitlab.com/tdillon/acurite-smarthub-node-proxy'
}

// Query parameters of the smartHUB request that contain weather data.
const smartHubWeatherParms = ['winddir', 'windspeedmph', 'humidity', 'baromin', 'tempf', 'rainin', 'dailyrainin']

const log = {};

app.get('/weatherstation/updateweatherstation', (req, res) => {
    log.date = new Date();

    // Remove keys from req.query that do not contain weather data.
    for (const key in req.query) {
        if (!smartHubWeatherParms.some(p => p === key)) {
            delete req.query[key];
        }
    }

    // Overlay  weather data from req.query onto parms to be sent to Weather Underground.
    Object.assign(apiUrlQueryParms, req.query);
    log.parms = apiUrlQueryParms

    // URL encode parms object
    let urlEncodedApiParms = Object.keys(apiUrlQueryParms).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(apiUrlQueryParms[k])}`).join('&');
    log.url = `${apiRootUrl}?${urlEncodedApiParms}`;

    // Send data to Weather Underground PWS API.
    request(`${apiRootUrl}?${urlEncodedApiParms}`, (error, response, body) => {
        log.error = error;
        log.statusCode = response && response.statusCode;
        log.body = body;
    });

    res.send({ localtime: new Date().toISOString().substring(11, 19) });
});

app.get('/status', (req, res) => {
    res.send(log);
})

http.listen(port, () => console.log(`listening on *:${port}`));
